const express = require('express');
const db = require("./database.js");

const router = express.Router();

router.get("/api/articles", (req, res, next) => {
    const sql = "select * from article";
    const params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(403).json({"error":err.message});
          return;
        }
        res.json({
            "message":"Успешно",
            "data":rows
        })
      });
});

router.get("/api/article/:id", (req, res, next) => {
    // Эта строка уязвима для SQL-инъекций
    // Для предотвращения стоит использовать вставку id через параметры:
    // const sql = 'select * from article where id = ?';
    // const params = [req.params.id];
    const sql = `select * from article where id = ${req.params.id}`;
    const params = [];
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(403).json({"error":err.message});
          return;
        }
        console.log('row: ', row);
        res.json({
            "message":"Успешно",
            "data":row
        });
      });
});

router.post("/api/article/", (req, res, next) => {
    const errors=[];
    // Если date - дата создания, то слать ее не нужно. Нужно настроить таблицу так, 
    // чтобы это поле создавалось автоматически, примерно так, как сейчас создается поле id.
    // Если это дата чего-то другого, то пропущена валидация поля date.
    if (!req.body.title){
        errors.push("title обязательно");
    }
    if (!req.body.body){
        errors.push("body обязателен");
    }
    if (errors.length){
        // join делать не нужно. Можно так и отправить массивом,
        // и слать не "error", а "errors"
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    const data = {
        title: req.body.title,
        body: req.body.body,
        date: req.body.date
    };
    const sql ='INSERT INTO article (title, body, date) VALUES (?,?,?)';
    const params =[data.title, data.body, data.date];
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(403).json({"error": err.message});
            return;
        }
        res.json({
            "message": "Успешно",
            "data": data,
            "id" : this.lastID
        });
    });
});

router.put("/api/article/:id", (req, res, next) => {
    const data = {
        title: req.body.title,
        body: req.body.body
    };
    console.log(data);
    db.run(
        `UPDATE article set 
           title = COALESCE(?,title),
           body = COALESCE(?,body)
           WHERE id = ?`,
        [data.title, data.body, req.params.id],
        (err, result) => {
            if (err){
                console.log(err);
                res.status(403).json({"error": res.message});
                return;
            }
            // Баг: в случае, когда входные данные неполные, выходные так же неполные.
            // То есть если входным патаметром будем только title, то сервер вернет нам только title,
            // а должен вернуть всю сущность.
            res.json({
                message: "Успешно",
                data: data
            });
    });
});

router.delete("/api/article/:id", (req, res, next) => {
    db.run(
        'DELETE FROM article WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(403).json({"error": res.message});
                return;
            }
            //В DELETE-маршрутах при успехе не надо отсылать никаких данных
            res.json({"message":"Удалено", rows: this.changes});
    });
});

// Не понятно, зачем нужен этот обработчик? Он не выполняет никакой полезной работы
// Если в требованиях о нем ничего не сказано, то его нужно удалить.
router.get("/", (req, res, next) => {
    res.json({"message":"Ok"});
});

module.exports = router;

// В текущем решении слово article используется в URL'ах как в единственном,
// так и множественном числе. Так не должно быть. Нужно везде
// использовать либо article либо articles

// Все коды ошибок 403 не верны. Вместо них должен быть код 500

// В DELETE и PUT запросах никак не обрабатывается ситуация, когда происходит запрос по id,
// которого не существует. В этом случае нужно отсылать ошибку 404

// Код загрязнен лишними console.log'ами - исправить

// При успешном выполнении операции:
// 1. Не надо слать никаких месседжей, это нужно только для ошибок
// 2. Не надо лишней волженности:
// Плохо:
// res.json({
//   data: data
// });
// Хорошо:
// res.json(data);
