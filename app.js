const express = require("express");
const bodyParser = require("body-parser");
const router = require('./src/router.js');

const app = express();
const HTTP_PORT = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', router);

// Start server
app.listen(HTTP_PORT, () => {
    console.log(`Server running on port ${HTTP_PORT}`);
});

// Поправить неоднородное форматирование. Где-то отступ 4 пробела, где-то 2, где-то это смешивается.
// Рекомендую везде использовать 2 отступа. Для исправления рекомендую работать в IDE (VSCode, Atom, IntelliJ),
// чтобы исправлять не вручную, а использовать встроенное средство автоформатирования.

// Видимо ошибка в именовании файла "_gitignore". Вместо нижнего подчеркивания должна быть точка.
